﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship {

	private int Identifier;
	private string FullName;
	private Texture2D ShipTexture;

	public Ship(int identifier,  string spriteName, string fullName) {
		this.Identifier = identifier;
		this.FullName = fullName;
		this.ShipTexture = Resources.Load<Texture2D>("images/" + spriteName);
	}

	public int getIdentifier()
	{
		return Identifier;
	}

	public string getFullName()
	{
		return FullName;
	}

	public Texture2D getShipTexture()
	{
		return ShipTexture;
	}
	
}

