﻿using UnityEngine;
using System.Collections;


public class Tile : BoardSquare
{
	public Ship ship;

	protected override void Start() {
		base.Start ();

		images[1] = ship.getShipTexture();
		this.GetComponent<Renderer>().material.mainTexture = images[1];
		textBox.GetComponent<TextMesh>().text = ship.getFullName();
	}
	
	protected override void onSquareSelected() {
		base.onSquareSelected();

		Invoke("resetSquare", 1f);
	}

	public override void changeSquareStatus(Color color, string str)
	{
		base.changeSquareStatus(color, str);
	}


	protected override void updateEndGraphic ()
	{
		base.updateEndGraphic();
	}
	
	public void tilePicked()
	{
		base.OnMouseDown();
	}
}